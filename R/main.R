#' Gillepsie SSA
#' @param K A Integer :
#' @param tmax A Integer :
#' @param Nmax A Integer :
#' @param No A Integer :
#' @param nrolls A Integer :
#' @param ES A Matrix :
#' @param b0 A Numeric :
#' @param d0 A Numeric :
#' @param intervalTime A Numeric : The interval time for the result
#' @param result_path A Integer : The path of results file (optional)
#' @param result_path_details A Integer : The path of results details files (optional)
#' @param print_debug A Logical : more verbose
RSSA <- function(K, tmax, Nmax, No, nrolls, ES, b0, d0, intervalTime = 0.1, result_path = "", result_path_details = "", print_debug = FALSE) {
  
    print("test 2")
    CPPSSA(K, tmax, Nmax, No, nrolls, ES, b0, d0, intervalTime, result_path, result_path_details, print_debug)
}


